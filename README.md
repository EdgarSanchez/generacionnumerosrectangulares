El algoritmo fue realizado en el lenguaje java con una biblioteca gráfica para Java denomina Swing.

La lógica extraída del libro para realizar:

**Congruencial Mixto**

La fórmula a usarse es: Xn+1 = ((a * Xn) + c) % m

Los parámetros para cada una de las variables son los siguientes:
*  a = 4*k +1, donde a debe ser un número entero impar mayor a 0.
*  m = p^d, donde p es nuestra base en binario (2) y d es el número de bits máximo.
*  c = se genera un número entero impar mayor a 0, más cercano al número primo, que a su vez este es el más cercano a m.
*  X0 = es un número entero cualesquiera.
*  a y c, deben ser primos entre sí, número coprimos o números primos relativos son dos o más números que no tienen más divisor común que 1.
*  Y finalmente m > X0, m > a y m > c

Los parámetros mencionados anteriormente se deben cumplir para que el generador resultante tenga período completo.

A continuación, se muestra el funcionamiento:
1.	Primero se muestra una pantalla donde se ingresa las variables
![Imagen 1.1](images/1.1.png)

2.	Si los valores son incorrectos pedirá nuevamente otro valor.
![Imagen 1.2](images/1.2.png)

3.	Una vez ingresado correctamente los datos y cumplan todos los parámetros se produce los datos.
![Imagen 1.3](images/1.3.png)

**Congruencial Multiplicativo**

La fórmula a usarse es: Xn+1 = (a * Xn) % m

Los parámetros para cada una de las variables son los siguientes:
*  a = 8*k ±3, donde a debe ser un número entero impar mayor a 0.
*  m = p^d, donde p es nuestra base en binario (2) y d es el número de bits máximo.
*  X0 = debe ser impar mayor a 0 y primo relativo con m.

Los parámetros mencionados anteriormente se deben cumplir para que el generador resultante tenga período completo.

A continuación, se muestra el funcionamiento:
1.	Primero se muestra una pantalla donde se ingresa las variables
![Imagen 2.1](images/2.1.png)

2.	Si los valores son incorrectos pedirá nuevamente otro valor.
![Imagen 2.2](images/2.2.png)

3.	Una vez ingresado correctamente los datos y cumplan todos los parámetros se produce los datos.
![Imagen 2.3](images/2.3.png) 


